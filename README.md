# subtitles_txt2ppt

Convert a text file to a powerpoint file

## Installation

You will need to install:

- Python
- pptx `pip install python-pptx`

## Usage

1. convert your speach in a text file knowing that:
   1. Each line return will start a new slide
   2. Use "###" to create an empty slide
2. choose the following parameters :
   1. max_line_length (corresponding to the maximum character to put in one slide)
   2. layout_id (corresponding to the id of the layout you defined in your ppt file) This layout should contain a "text holder" where the text will be filled.
3. run `python subtitles.py 'path_to_text.txt' 'path_to_ppt_layout.ppt' 'output_name.ppt'`

Try it `python subtitles.py speach.txt speach_layout.pptx -n 0 -m 150 -o output.ppt`

## Support

Open an issue in this repo
