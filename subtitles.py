import argparse
import textwrap
from pathlib import Path

from pptx import Presentation


def main(args):
    text_file = Path(args.text)
    layout_file = Path(args.layout)
    layout_nb = args.num
    max_len = args.max_len
    output_file = Path(args.out_dir)

    a = open(text_file, "r")
    prs = Presentation(layout_file)
    layout = prs.slide_layouts[layout_nb]

    for i in a:
        if "###" in i: # new paragraph
            prs.slides.add_slide(layout)
        if len(i.split()) > 1:
            lines = textwrap.wrap(i, max_len, break_long_words=False)
            for line in lines:
                prs.slides.add_slide(layout)
                prs.slides[-1].placeholders[0].text = line

    prs.save(output_file)


if __name__ == '__main__':
    desc = "convert a text.txt to a powerpoint.ppt using a ppt layout"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('text', type=Path,
                        help="the speach text to convert.'###' will create an empty slide. "
                        "Any return line will create a new slide.")
    parser.add_argument('layout', type=Path,
                        help="the mock ppt file with the layout to use.")
    parser.add_argument('-n', '--num', type=int, default=0,
                        help="the layout number in the layout to use. Default is 0.")
    parser.add_argument('-m', '--max-len', type=int, default=150,
                        help="the maximum string length to put in one slide. "
                        "default=150")
    parser.add_argument('-o', '--out-dir', type=Path, default='output.ppt',
                        help="Give an explicit output ppt name. Default 'output.ppt'.")
    args = parser.parse_args()

    main(args)
